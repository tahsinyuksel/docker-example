# Notlar

# nginx başlatma

docker run -v /Users/jokerdev1/Sites/public/dockerPublic:/usr/share/nginx/html --name docker-nginx -p 8080:80 nginx

Not: -v volume run komutundan hemen sonra yer almalı. komutun sonlarında yer alırsa çalışmıyor.
https://stackoverflow.com/questions/27158840/docker-executable-file-not-found-in-path

Hata: docker: Error response from daemon: OCI runtime create failed: container_linux.go:345: starting container process caused "exec: \"-v\": executable file not found in $PATH": unknown.
ERRO[0000] error waiting for container: context canceled 

Bulunduğun dizinden volume vererek çalıştırma
docker run -v $(pwd):/src -it <container_name>

# docker mysql bağlantı
mysql kurulduğunda bağlantı sağlanamadı.container içinde mysql php tarafından connection olmadı.
Bunun için mysql de kullanıcı ve hosta erişim tanımlamamız gerekiyor.
The server requested authentication method unknown to the client (PHP) hatası
https://stackoverflow.com/questions/52364415/the-server-requested-authentication-method-unknown-to-the-client-php

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'pass';
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'pass';
ALTER USER 'default'@'%' IDENTIFIED WITH mysql_native_password BY 'pass';

# How to connect PHP container with MySql container
php container dan mysql container daki mysql bağlanma için;
mysql container ip adresinin kullanılması gerekir. localhost yada 127.0.0.1 adresleri her bir container ın kendi local ortamını ifade eder. Oysaki mysql başka container da çalıştığı için php container localda mysql bulamaz bu sebeple bağlanmaz. mysql container ip adresini kullanarak bağlantı sağlanır.
mysql container ip öğrenmek için;
docker inspect web-mysql | grep IPAddress

docker ps
container id veya adı öğrenilir.

docker exec -it web-mysql bash
ile container bağlanılır.

mysql erişilir.

# Failed to connect to MySQL: No such file or directory
https://unix.stackexchange.com/questions/432976/failed-to-connect-to-mysql-no-such-file-or-directory

Muhtemelen mysql olmayan bir container dan erişmeye çalışıyorsunuz. Örneğin php den bağlanmaya çalışıyorsunuz ancak Mysql başka bir container olarak çalıştığı için localhost veya 127.0.0.1 gibi host ile erişmeye çalıştığınızda alabilirsiniz bu hatayı. 
Bunun yerine mysql container IP adresini kullanın. 
Bir diğer yöntem ise docker-compose da containerları aynı networke dahil etmeniz. Böylece aynı networkdeki tanımladığınız container ile direkt host olarak kullanabilirsiniz. Örnek;
$mysqlsunucu = "web-mysql";

örnek çalışma:
https://stackoverflow.com/questions/43127599/connect-to-mysql-server-from-php-with-docker

# localden container daki mysql bağlantı kurma
docker-machine ip ile docker makine ip öğrenilir.
tanımlanan bilgi ve ip ile erişim sağlanır.
https://stackoverflow.com/questions/33827342/how-to-connect-mysql-workbench-to-running-mysql-inside-docker

volume kullanırken
mysql 8.0.16 de bir bug kaynaklı durumdan dolayı host makineden docker daki mysql bağlantı sağlanamıyordu. my.cnf dosyasına innodb_use_native_aio=0 eklemeyle hem bu sorun hem de "Operating system error number 22 in a file operation" gibi hatalar çözülmüş oluyor.
https://github.com/docker-library/mysql/issues/365
https://stackoverflow.com/questions/48400495/docker-compose-mysql-aio-write-error-w10

# initialize specified but the data directory has files in it. Aborting.
volume de belirtilen dizinin boş olmadığını, ya dizin içeriğini boşaltmanız - silmeniz ya da yeni bir path veya volume belirtmeniz gerektiğini söyler.
https://stackoverflow.com/questions/37644118/initializing-mysql-directory-error

# 
docker mysql can't create/write to file permission denied
DB volume dizinine yetki verilmesi gerekiyor. Docker çalıştıran kullanıcı için.
sudo chown $(whoami) ./data/db

dockerfile da şöyle bir tanımlama yapmak gerekebilir.
https://github.com/maxpou/docker-symfony/issues/19
FROM mysql

RUN usermod -u 1000 mysql

# container listeleme
docker ps -a

docker ps

# Volume listesi

docker volume ls 

# Container silme

docker rm docker-nginx 

docker rm 231sfda2

docker rm -v docker-nginx 
Volume ile birlikte siler

Durdurmaya zorla ve sil
docker kill 123saf23r

# Container başlatma durdurma

docker start 123saf23r
docker stop 123saf23r


# uzaktan mysql erişme
https://cyberomin.github.io/tutorial/docker/2016/01/12/docker-mysql.html

# Container bash connection error
docker exec -it web-redis bash  

OCI runtime exec failed: exec failed: container_linux.go:345: starting container process caused "exec: \"bash\": executable file not found in $PATH": unknown

Solution:
docker exec -it web-redis sh 

Link: https://stackoverflow.com/questions/42044959/how-to-ssh-docker-container

# 
ERROR: Service 'php' failed to build: error creating overlay mount to /mnt/sda1/var/lib/docker/overlay2/2a54a20c68cd30ee932eebd327a19fae2bc7fb5c6eeea28dc668c317809bc1fd-init/merged: no such file or directory

Use Try:
docker system prune -af 