# Docker Example

Base Nginx - PHP - MySQL docker working environment in example

## Step 1: Compose build

docker-compose build

## Step 2: Compose up - run

docker-compose up

# Directories

## Nginx

Server definations
default.conf

Ports: 
Dışarıdan Host 8080 portu container içinde 80 portuna yönlendirildi. Böylece host da 8080 istek yapıldığında container aktarılacaktır.

Volumes:
"./:/usr/share/nginx/html" ile çalışma dizini, nginx container daki html dizinine eşlendi. "./" releative path olarak çalışma dizinini ifade eder.

"./nginx/default.conf:/etc/nginx/conf.d/default.conf" çalışma dizini nginx klasöründe yer alan nginx conf dosyasını container da nginx conf diziniyle eşleştiriyoruz. Böylece nginx container daki conf dosyası bizim kontrolümüzde, çalışma dizinimizde.

links:
Web sunucusu PHP imajıyla iletişimde olması için iki servis ilişkilendirilir.

networks:
Imageların aynı networkde olması sağlanır. Böylece image isimleride uygulamalarda kullanılabilir.

## Logs
Web access and error logs

## Php
PDO and others install

volumes:
PHP-FPM default yolu ile ilişkilendirildi.

build:
php imagına özel tanımlamaların olduğu dockerfile dosya yolu tanımlanır.

links:
mysql ile ilişkilendirilir.

## MySql
DB settings

## Data
Persist data in container for MySql database

# Example

find docker ip
docker-machine ip 

docker ip (ex: 192.168.99.100)

http://192.168.99.100/
call index.html (nginx default use index.html)

http://192.168.99.100/about.html
call about.html

http://192.168.99.100/index.php
call index.php

http://192.168.99.100/example.php
call example.php (for database connection example)


# Docs & Notes

[notes](docs/)

